chart: k8s-at-home/frigate
version: ~6
namespace: default
values:
- env:
    TZ: {{ .Values.timezone }}
  image:
    tag: stable-aarch64
  ingress:
    main:
      enabled: true
      hosts:
        - host: frigate.{{ .Values.hostname }}
          paths:
            - path: /
  coral:
    enabled: true
  securityContext:
    privileged: true
  persistence:
    data:
      enabled: true
      accessMode: ReadWriteOnce
      size: 1G
    media:
      enabled: true
      accessMode: ReadWriteOnce
      size: 10G
    usb:
      enabled: true
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
          - matchExpressions:
              - key: feature.node.kubernetes.io/usb-fe_1a6e_089a.present
                operator: Exists
          - matchExpressions:
              - key: feature.node.kubernetes.io/usb-ff_18d1_9302.present
                operator: Exists
  config: |
    mqtt:
      host: mosquitto
    detectors:
      coral:
        type: edgetpu
        device: usb
    ffmpeg:
      #hwaccel_args:
      #  - -c:v
      #  - h264_v4l2m2m
      output_args:
        # remove -an for audio support
        record: -f segment -segment_time 60 -segment_format mp4 -reset_timestamps 1 -strftime 1 -c copy
    detect:
      width: 704
      height: 480
      fps: 5
    record:
      enabled: true
      retain_days: 0
      events:
        retain:
          default: 10
    snapshots:
      enabled: true
    cameras:
      front_yard:
        ffmpeg:
          inputs:
            - path: rtsp://{{ exec "bw" (list "get" "username" "front_yard") }}:{{ exec "bw" (list "get" "password" "front_yard") }}@front_yard:554/cam/realmonitor?channel=1&subtype=1
              roles:
                - detect
                - record
                - rtmp
      back_yard:
        ffmpeg:
          inputs:
            - path: rtsp://{{ exec "bw" (list "get" "username" "front_yard") }}:{{ exec "bw" (list "get" "password" "front_yard") }}@back_yard:554/cam/realmonitor?channel=1&subtype=1
              roles:
                - detect
                - record
                - rtmp
      doorbell:
        detect:
          width: 800
          height: 480
          fps: 5
        ffmpeg:
          # add pcm_s16be codec
          output_args:
            record: -f segment -segment_time 60 -segment_format mp4 -reset_timestamps 1 -strftime 1 -c copy -acodec pcm_s16be
          inputs:
            - path: rtsp://{{ exec "bw" (list "get" "username" "doorbell") }}:{{ exec "bw" (list "get" "password" "doorbell" )}}@doorbell:554/cam/realmonitor?channel=1&subtype=1
              roles:
                - detect
                - record
                - rtmp
  